Document: debian-history.fr
Title: A Short History of the Debian Project, French translation
Author: Bdale Garbee, Patrice Karatchentzeff and others
Abstract: A Short History of the Debian Project
Section: Debian

Format: HTML
Index: /usr/share/doc/debian-history/docs/index.fr.html
Files: /usr/share/doc/debian-history/docs/*.fr.html

Format: pdf
Files: /usr/share/doc/debian-history/docs/*.fr.pdf

Format: text
Files: /usr/share/doc/debian-history/docs/*.fr.txt.gz
